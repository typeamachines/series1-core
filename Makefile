default:
	@echo Try 'make all'

all: snapshot-changelog deb

snapshot-changelog:
	git dch --verbose --ignore-branch --snapshot --auto debian/

release-changelog:
	git dch --verbose --ignore-branch --release --spawn-editor=snapshot --auto \
		debian/

deb:
	git buildpackage \
	--git-ignore-new \
	--git-upstream-tree=HEAD \
	-us -uc

deb-tag:
	git buildpackage \
	--git-ignore-new \
	--git-tag-only \
